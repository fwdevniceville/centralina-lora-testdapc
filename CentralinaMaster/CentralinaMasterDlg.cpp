
// CentralinaMasterDlg.cpp: file di implementazione
//

#include "stdafx.h"
#include "CentralinaMaster.h"
#include "CentralinaMasterDlg.h"
#include "afxdialogex.h"
#include "crc8.h"
#include "../../rapidjson/document.h"
#include <string>
#include <time.h>
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// finestra di dialogo CAboutDlg utilizzata per visualizzare le informazioni sull'applicazione.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dati della finestra di dialogo
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // supporto DDX/DDV

// Implementazione
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// Finestra di dialogo CCentralinaMasterDlg



CCentralinaMasterDlg::CCentralinaMasterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(IDD_CENTRALINAMASTER_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_iMsgProgr = 0;
	mqtt_client = NULL;
}
CCentralinaMasterDlg::~CCentralinaMasterDlg()
{

}

void CCentralinaMasterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CCentralinaMasterDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_SUBSCRIBE, &CCentralinaMasterDlg::OnBnClickedButtonSubscribe)
	ON_STN_CLICKED(IDC_TEXT_SUB_RX, &CCentralinaMasterDlg::OnStnClickedTextSubRx)
	ON_BN_CLICKED(IDC_BUTTON_SEND_TIME, &CCentralinaMasterDlg::OnBnClickedButtonSendTime)
	ON_BN_CLICKED(IDC_BUTTON_VALVE_OPEN, &CCentralinaMasterDlg::OnBnClickedButtonValveOpen)
	ON_BN_CLICKED(IDC_BUTTON_VALVE_CLOSE, &CCentralinaMasterDlg::OnBnClickedButtonValveClose)
	ON_BN_CLICKED(IDC_BUTTON_SCHEDULE_READ, &CCentralinaMasterDlg::OnBnClickedButtonScheduleRead)
	ON_BN_CLICKED(IDC_BUTTON_SCHEDULE_WRITE, &CCentralinaMasterDlg::OnBnClickedButtonScheduleWrite)
END_MESSAGE_MAP()


// Gestori di messaggi di CCentralinaMasterDlg

BOOL CCentralinaMasterDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Aggiungere la voce di menu "Informazioni su..." al menu di sistema.

	// IDM_ABOUTBOX deve trovarsi tra i comandi di sistema.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Impostare l'icona per questa finestra di dialogo.  Il framework non esegue questa operazione automaticamente
	//  se la finestra principale dell'applicazione non è una finestra di dialogo.
	SetIcon(m_hIcon, TRUE);			// Impostare icona grande.
	SetIcon(m_hIcon, FALSE);		// Impostare icona piccola.

	// TODO: aggiungere qui inizializzazione aggiuntiva.




	MQTTAsync_createOptions		createOptions = MQTTAsync_createOptions_initializer;
	int rc = 0;


	rc = MQTTAsync_createWithOptions (&mqtt_client, "localhost:1883", "pirupii", MQTTCLIENT_PERSISTENCE_DEFAULT, NULL, &createOptions);
//	rc = MQTTAsync_createWithOptions (&mqtt_client, "https://52.15.232.73:1883", "pirupii", MQTTCLIENT_PERSISTENCE_DEFAULT, NULL, &createOptions);

	if (rc == MQTTASYNC_SUCCESS)
	{
		rc = MQTTAsync_setCallbacks(mqtt_client, this, NULL, mqtt_client_message_recv, NULL);
	}
	if (rc == MQTTASYNC_SUCCESS)
	{
		MQTTAsync_connectOptions	opts = MQTTAsync_connectOptions_initializer;

		opts.keepAliveInterval = 20;
		opts.cleansession = 1;
		opts.automaticReconnect = 1;
		//opts.username = "testuser";
		//opts.password = "testpassword";

		opts.will = NULL; /* don't need will for this client, as it's going to be connected all the time */
		opts.context = this;
		opts.onSuccess = NULL;// test1dOnConnect;
		opts.onFailure = NULL;//test1dOnFailure;

		rc = MQTTAsync_connect(mqtt_client, &opts);

	}
	if (rc == MQTTASYNC_SUCCESS)
	{

	}

	m_strDEVUI = "0102030405060708";

	
	return TRUE;  // restituisce TRUE a meno che non venga impostato lo stato attivo su un controllo.
}

#include "lora_payload.h"

#define CENTRALINA_LORAWAN_ANSW_STATUS_SYS	(1) // size 1 + 2
#define CENTRALINA_LORAWAN_ANSW_STATUS_OUTS	(2) // size 1 + 2
#define CENTRALINA_LORAWAN_ANSW_CHECK_SCHED	(3) // size 1 + 2
#define CENTRALINA_LORAWAN_ANSW_STATUS_INS	(4) // size 1 + 1
#define CENTRALINA_LORAWAN_ANSW_STATUS_TIME	(5) // size 1 + ?

#include "CBitPackerUnpaker.h"
#include "base64.h"

int CCentralinaMasterDlg::mqtt_client_message_recv(void* context, char* topicName, int topicLen, MQTTAsync_message* message)
{
	uint8_t	auiBinaryPayload[1024];
	char	strRow[256];

	message->msgid;
	message->payload;
	message->payloadlen;
	message->qos;
	message->retained;

	rapidjson::Document		docLoraFrame;
	rapidjson::ParseResult	pResult;

	std::string	strRet;
	std::string strMessage;


	strRet.assign((char*)(message->payload), message->payloadlen);

	pResult = docLoraFrame.Parse(strRet.c_str());
	strRet = "parseFailure";
	if (pResult)
	{
		int iPOrt;
		std::string strPort;
		std::string strPayLoad;
		iPOrt = docLoraFrame["fPort"].GetInt ();
		strPort = docLoraFrame["fPort"].GetInt ();
		strPayLoad = docLoraFrame["data"].GetString ();

		_itoa_s (iPOrt, (char*)auiBinaryPayload, 1024, 10);
		strRet = "Port:";
		strRet += (char*)auiBinaryPayload;
		strRet += " data: ";
		strRet += strPayLoad;
		bool	bComplete;
		int iPayloadByteSize;
		CBitPackerUnpaker	bitPacker;

		iPayloadByteSize = base64_decode ((uint8_t*)strPayLoad.c_str () , strlen (strPayLoad.c_str ()), auiBinaryPayload);

		if (iPayloadByteSize > 0)
		{
			int				iSCNR;
			int				iPSZ;
			unsigned		uiTMP;
			const uint8_t*	aBLOCK;
			iSCNR = 0;
			bitPacker.SetPopBuffer (auiBinaryPayload, iPayloadByteSize);
			switch (iPOrt)
			{
			case CENTRALINA_LORAWAN_PORT_CMD_RET_DATA:
				{
					while (iSCNR < iPayloadByteSize)
					{
						iPSZ = iPayloadByteSize - iSCNR;

						aBLOCK = &auiBinaryPayload[iSCNR];
						switch (aBLOCK[0])
						{
						case CENTRALINA_LORAWAN_ANSW_STATUS_SYS:
							{
								if (iPSZ >= 3)
								{
									sprintf_s (strRow, sizeof (strRow), "STATO (%02X-%d)\n", (int)aBLOCK[1], (int)aBLOCK[2]);
									strMessage += strRow;
									aBLOCK[1]; //stato

									if (aBLOCK[1] & 0x08)
									{
										// in schedulazione
									}
									aBLOCK[2]; // schedulazione

									iPSZ = 3;
								}
								break;
							}
						case CENTRALINA_LORAWAN_ANSW_STATUS_OUTS:
							{
								if (iPSZ >= 3)
								{
									sprintf_s (strRow, sizeof (strRow), "USCITE DRV = %02X KO = %02X\n",(int)aBLOCK[1], (int)aBLOCK[2]);
									strMessage += strRow;
									aBLOCK[1]; //pilotate
									aBLOCK[2]; //anomale

									iPSZ = 3;
								}
								break;
							}
						case CENTRALINA_LORAWAN_ANSW_CHECK_SCHED:
							{
								if (iPSZ >= 3)
								{

									if (aBLOCK[1] & 0x80)
									{
										sprintf_s (strRow, sizeof (strRow), "SCHEDULAZIONE %d : ASSENTE (%02X)\n", (int)(aBLOCK[1] & 0x7F), (int)aBLOCK[2]);
									}
									else
									{
										sprintf_s (strRow, sizeof (strRow), "SCHEDULAZIONE %d : PRESENTE CRC8 = %02X\n", (int)(aBLOCK[1] & 0x7F), (int)aBLOCK[2]);
									}
									strMessage += strRow;

									aBLOCK[1]; // numero schedulazione
									if (aBLOCK[1] & 0x80)
									{
										// schedulazione mancante
									}
									aBLOCK[2]; // crc8 ricevuto dal server
									iPSZ = 3;
								}
								break;
							}
						}

						iSCNR += iPSZ;
					}

				}
				break;
			}

		}

	}

	::SetDlgItemTextA(((CCentralinaMasterDlg*)context)->m_hWnd,IDC_TEXT_SUB_RX, strRet.c_str());
	::SetDlgItemTextA(((CCentralinaMasterDlg*)context)->m_hWnd,IDC_TEXT_SUB_RX_DEC, strMessage.c_str());
	
	return 1;
	
}

void CCentralinaMasterDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// Se si aggiunge alla finestra di dialogo un pulsante di riduzione a icona, per trascinare l'icona sarà necessario
//  il codice sottostante.  Per le applicazioni MFC che utilizzano il modello documento/visualizzazione,
//  questa operazione viene eseguita automaticamente dal framework.

void CCentralinaMasterDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // contesto di dispositivo per il disegno

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Centrare l'icona nel rettangolo client.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Disegnare l'icona
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Il sistema chiama questa funzione per ottenere la visualizzazione del cursore durante il trascinamento
//  della finestra ridotta a icona.
HCURSOR CCentralinaMasterDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CCentralinaMasterDlg::OnDestroy()
{
	if (mqtt_client != NULL)
	{
		MQTTAsync_disconnectOptions	opts = MQTTAsync_disconnectOptions_initializer;
		

		MQTTAsync_disconnect(mqtt_client,&opts);
		MQTTAsync_destroy(&mqtt_client);

	}
	CDialog::OnDestroy();

	// TODO: aggiungere qui il codice del gestore di messaggi
}


void CCentralinaMasterDlg::OnBnClickedButtonSubscribe()
{
	// TODO: aggiungere qui il codice del gestore di notifiche del controllo
	if (mqtt_client != NULL)
	{
		MQTTAsync_responseOptions ropt = MQTTAsync_responseOptions_initializer;
		int rc;
		CString	strPath;
		CString	strTemp;

		
		m_strDEVUI = "0000";
		GetDlgItemText (IDC_EDIT_CUST_ID_2, strTemp);
		m_strDEVUI += strTemp;
		GetDlgItemText (IDC_EDIT_CUST_ID_1, strTemp);
		m_strDEVUI += strTemp;
		GetDlgItemText (IDC_EDIT_NODE_ID, strTemp);
		m_strDEVUI += strTemp;

		strPath = "application/1/node/";
		strPath += m_strDEVUI;
		strPath += "/rx";
		rc = MQTTAsync_subscribe(mqtt_client, strPath, 0, &ropt);
	}
}


void CCentralinaMasterDlg::OnStnClickedTextSubRx()
{
	// TODO: aggiungere qui il codice del gestore di notifiche del controllo
}


void CCentralinaMasterDlg::OnBnClickedButtonSendTime()
{
	// TODO: aggiungere qui il codice del gestore di notifiche del controllo
	unsigned sizBuffer;
	uint8_t	auiBinaryPayload[1024];
	CBitPackerUnpaker	bitPacker;


	time_t rawtime;
	struct tm  ptm;

	time (&rawtime);

	gmtime_s (&ptm,&rawtime);

	int iYY;
	int iMO;
	int iDD;
	int iWD;
	int iHH;
	int iMM;

	iYY = ptm.tm_year;
	iYY -= 100;
	iMO = ptm.tm_mon + 1;
	iDD = ptm.tm_mday;
	iWD = (ptm.tm_wday ? ptm.tm_wday : 7);

	iHH = ptm.tm_hour;
	iMM = ptm.tm_min;


	auiBinaryPayload[0] = m_iMsgProgr;
	auiBinaryPayload[1] = CENTRALINA_LORAWAN_CMD_DATETIME;
	auiBinaryPayload[2] = iYY;
	auiBinaryPayload[3] = iMO;
	auiBinaryPayload[4] = iDD;
	auiBinaryPayload[5] = iWD;
	auiBinaryPayload[6] = iHH;
	auiBinaryPayload[7] = iMM;

	sizBuffer = 1 + 1 + 6;

	SendLoRaWANMessage (m_strDEVUI, NULL, true, CENTRALINA_LORAWAN_PORT_CMD, auiBinaryPayload, sizBuffer);
}


void CCentralinaMasterDlg::OnBnClickedButtonValveOpen()
{
	// TODO: aggiungere qui il codice del gestore di notifiche del controllo
	uint8_t	auiBinaryPayload[1024];
	unsigned sizBuffer;
	int		iValvola;

	iValvola = GetDlgItemInt (IDC_EDIT_VALVE_NUM, NULL, FALSE);
	if ((iValvola > 0) && (iValvola <= 4))
	{
//		iValvola--;
		auiBinaryPayload[0] = m_iMsgProgr;
		auiBinaryPayload[0] = CENTRALINA_LORAWAN_RET_MANUAL_OUT;
		auiBinaryPayload[1] = iValvola;
		auiBinaryPayload[2] = 2;
		sizBuffer = 1 + 1 + 2;
		SendLoRaWANMessage (m_strDEVUI, NULL, true, CENTRALINA_LORAWAN_PORT_CMD, auiBinaryPayload, sizBuffer);
	}
}


void CCentralinaMasterDlg::OnBnClickedButtonValveClose()
{
	// TODO: aggiungere qui il codice del gestore di notifiche del controllo
	uint8_t	auiBinaryPayload[1024];
	unsigned sizBuffer;
	int		iValvola;

	iValvola = GetDlgItemInt (IDC_EDIT_VALVE_NUM, NULL, FALSE);
	if ((iValvola > 0) && (iValvola <= 4))
	{
//		iValvola--;
		auiBinaryPayload[0] = m_iMsgProgr;
		auiBinaryPayload[0] = CENTRALINA_LORAWAN_RET_MANUAL_OUT;
		auiBinaryPayload[1] = iValvola;
		auiBinaryPayload[2] = 0;
		sizBuffer = 1 + 1 + 2;
		SendLoRaWANMessage (m_strDEVUI, NULL, true, CENTRALINA_LORAWAN_PORT_CMD, auiBinaryPayload, sizBuffer);
	}
}


void CCentralinaMasterDlg::OnBnClickedButtonScheduleRead()
{
	// TODO: aggiungere qui il codice del gestore di notifiche del controllo
}


void CCentralinaMasterDlg::OnBnClickedButtonScheduleWrite()
{
	// TODO: aggiungere qui il codice del gestore di notifiche del controllo
	// schedulazione programmata
	unsigned uiSchedulazione;
	unsigned uiModo;
	unsigned uiAnno;
	unsigned uiMese;
	unsigned uiGiorno;
	unsigned uiPeriodo;
	unsigned uiStartOra;
	unsigned uiStartMinuti;
	unsigned uiDurataMinuti;
	unsigned uiValvoleAccese;
	uint8_t	auiBinaryPayload[1024];
	CBitPackerUnpaker	bitPacker;

	uiSchedulazione = GetDlgItemInt (IDC_EDIT_SHEDULE_NUMBER);
	uiModo = GetDlgItemInt (IDC_EDIT_MODE);
	uiAnno = GetDlgItemInt (IDC_EDIT_SANNO);
	uiMese = GetDlgItemInt (IDC_EDIT_SMONTH);
	uiGiorno = GetDlgItemInt (IDC_EDIT_SDAY);
	uiPeriodo = GetDlgItemInt (IDC_EDIT_PDAY);
	uiStartOra = GetDlgItemInt (IDC_EDIT_SORA_START);
	uiStartMinuti = GetDlgItemInt (IDC_EDIT_MIN_START);
	uiDurataMinuti = GetDlgItemInt (IDC_EDIT_MIN_RUN);
	uiValvoleAccese = 0;
	if (IsDlgButtonChecked (IDC_CHECK_SCHED_V1))
	{
		uiValvoleAccese |= 0x1;
	}
	if (IsDlgButtonChecked (IDC_CHECK_SCHED_V2))
	{
		uiValvoleAccese |= 0x2;
	}
	if (IsDlgButtonChecked (IDC_CHECK_SCHED_V3))
	{
		uiValvoleAccese |= 0x4;
	}
	if (IsDlgButtonChecked (IDC_CHECK_SCHED_V4))
	{
		uiValvoleAccese |= 0x8;
	}

	size_t	sizBuffer;
	const uint8_t* abyBuffer;
	abyBuffer = auiBinaryPayload;

	if (1)
	{
		auiBinaryPayload[0] = m_iMsgProgr;
		auiBinaryPayload[1] = CENTRALINA_LORAWAN_CMD_PROGRAM_PACKED;
		 
		bitPacker.SetPushBuffer (&auiBinaryPayload[2], 8);
		bitPacker.pushUIntValue (uiSchedulazione, 7);
		bitPacker.pushUIntValue (uiModo, 3);
		bitPacker.pushUIntValue (uiAnno, 7);
		bitPacker.pushUIntValue (uiMese, 4);
		bitPacker.pushUIntValue (uiGiorno, 5);
		bitPacker.pushUIntValue (uiPeriodo, 8);
		bitPacker.pushUIntValue (uiStartOra, 5);
		bitPacker.pushUIntValue (uiStartMinuti, 6);
		bitPacker.pushUIntValue (uiDurataMinuti, 11);
		bitPacker.pushUIntValue (uiValvoleAccese, 8);

		auiBinaryPayload[10] = calcCRC8 (0xFF, &auiBinaryPayload[2], 8);
		sizBuffer = 1 + 1 + 8 + 1;
	}
	else
	{
		auiBinaryPayload[0] = m_iMsgProgr;
		auiBinaryPayload[1] = CENTRALINA_LORAWAN_CMD_PROGRAM;
		auiBinaryPayload[2] = uiSchedulazione;
		auiBinaryPayload[3] = uiModo;
		auiBinaryPayload[4] = uiAnno;
		auiBinaryPayload[5] = uiMese;
		auiBinaryPayload[6] = uiGiorno;
		auiBinaryPayload[7] = uiPeriodo;
		auiBinaryPayload[8] = uiStartOra;
		auiBinaryPayload[9] = uiStartMinuti;
		auiBinaryPayload[10] = uiDurataMinuti >> 8;
		auiBinaryPayload[11] = uiDurataMinuti;
		auiBinaryPayload[12] = uiValvoleAccese;
		auiBinaryPayload[13] = calcCRC8 (0xFF, &auiBinaryPayload[2], 11);

		sizBuffer = 1 + 1 + 11 + 1;
	}

	SendLoRaWANMessage (m_strDEVUI, NULL, true, CENTRALINA_LORAWAN_PORT_CMD, abyBuffer, sizBuffer);

}



#include "base64.h"

int CCentralinaMasterDlg::SendLoRaWANMessage (const char* strDevEUI, const char* strRefMsg, bool bConfirmed, unsigned int uiPort, const uint8_t* abyPayloadBuffer, unsigned int uiPayloadSize)
{
	char szTempBuffer[16];
	uint8_t szBase64Payload[512];
	char szMsgRefInternal[512];
	std::string	strMessage;
	int iSendRet;

	m_iMsgProgr++;
	if (strRefMsg == NULL)
	{
		sprintf_s (szMsgRefInternal, 512, "jdi9je33%Xsl29", m_iMsgProgr);
		strRefMsg = szMsgRefInternal;
	}

	memset (szBase64Payload, 0, sizeof (szBase64Payload));

	base64_encode_array (abyPayloadBuffer, uiPayloadSize, szBase64Payload);
	_itoa_s (uiPort, szTempBuffer, 16 , 10);
	strMessage = "{\"reference\" : \"";
	strMessage += strRefMsg;
	strMessage += "\", \"confirmed\" : ";
	strMessage += bConfirmed ? "true" : "false";
	strMessage +=", \"fPort\" : ";
	strMessage += szTempBuffer;
	strMessage += ", \"data\" : \"";
	strMessage += (char*)szBase64Payload;
	strMessage += "\" }";

	CString	strPath;
	strPath = "application/1/node/";
	strPath += m_strDEVUI;
	strPath += "/tx";

	MQTTAsync_responseOptions ropt = MQTTAsync_responseOptions_initializer;

	iSendRet = MQTTAsync_send (mqtt_client, strPath, strMessage.length (), (void*)strMessage.c_str (), 0, 0, &ropt);
	return iSendRet;

}
//DLLExport int MQTTAsync_send (MQTTAsync handle, const char* destinationName, int payloadlen, void* payload, int qos, int retained,

							  //DLLExport int MQTTAsync_sendMessage (MQTTAsync handle, const char* destinationName, const MQTTAsync_message* msg, MQTTAsync_responseOptions* response);
