#pragma once

size_t base64_encode_array (const uint8_t *instr, int size, uint8_t *outstr);
int base64_decode (const uint8_t *instr, int size, uint8_t *outstr);
