#pragma once

#include <stdint.h>
#include "bitpacker.h"

class CBitPackerUnpaker
{
public:
	CBitPackerUnpaker();
	virtual ~CBitPackerUnpaker();
public:
	void SetPushBuffer (uint8_t* abyBuffer, size_t uiSize);
	void SetPopBuffer (const uint8_t* abyBuffer, size_t uiSize);

	void pushIntValue(int iVal, size_t uiBitSize);
	void pushUIntValue(unsigned uiVal, size_t uiBitSize);

	void popIntValue(int* piVal, size_t uiBitSize);
	void popUIntValue(unsigned* puiVal, size_t uiBitSize);

	bool popPending (void)
	{
		return m_stBITPACKER.iCurrBit < m_stBITPACKER.iMaxBit ? true : false;
	}
public:
	size_t GetBufferLength () const
	{
		return bitpackerGetPushedBytes (&m_stBITPACKER);
	}

protected:
	BITPACKER				m_stBITPACKER;
};

