//{{NO_DEPENDENCIES}}
// File di inclusione generato con Microsoft Visual C++.
// Utilizzato da CentralinaMaster.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_CENTRALINAMASTER_DIALOG     102
#define IDR_MAINFRAME                   128
#define IDC_BUTTON_SUBSCRIBE            1000
#define IDC_TEXT_SUB_RX                 1001
#define IDC_EDIT_SHEDULE_NUMBER         1002
#define IDC_BUTTON_SEND_TIME            1003
#define IDC_EDIT_VALVE_NUM              1004
#define IDC_TEXT_SUB_RX2                1005
#define IDC_TEXT_SUB_RX_DEC             1005
#define IDC_BUTTON_VALVE_OPEN           1006
#define IDC_BUTTON_VALVE_CLOSE          1007
#define IDC_BUTTON_SCHEDULE_READ        1008
#define IDC_BUTTON_SCHEDULE_WRITE       1009
#define IDC_EDIT_MODE                   1010
#define IDC_EDIT_SDAY                   1011
#define IDC_EDIT_PDAY                   1012
#define IDC_EDIT_MIN_RUN                1013
#define IDC_EDIT_MIN_START              1014
#define IDC_EDIT_ORA_START              1014
#define IDC_EDIT_SORA_START             1014
#define IDC_CHECK_SCHED_V1              1015
#define IDC_CHECK_SCHED_V2              1016
#define IDC_CHECK_SCHED_V3              1017
#define IDC_CHECK_SCHED_V4              1018
#define IDC_EDIT_CUST_ID_1              1019
#define IDC_EDIT_CUST_ID_2              1020
#define IDC_EDIT_NODE_ID                1021
#define IDC_EDIT_NODE_ID2               1022
#define IDC_EDIT_SERVER_IP              1022
#define IDC_EDIT_SANNO                  1023
#define IDC_EDIT_SMONTH                 1024
#define IDC_EDIT_SMIN_START2            1025

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1016
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
