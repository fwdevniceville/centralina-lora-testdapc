
#include <stdint.h>
#include "base64.h"

static const char cb64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

static const char cd64[] = "|$$$}rstuvwxyz{$$$$$$$>?@ABCDEFGHIJKLMNOPQRSTUVW$$$$$$XYZ[\\]^_`abcdefghijklmnopq";

/**
* encode 3 8-bit binary bytes as 4 '6-bit' characters
*/
static void encodeblock (uint8_t in[3], uint8_t out[4], int len)
{

	out[0] = cb64[in[0] >> 2];
	out[1] = cb64[((in[0] & 0x03) << 4) | ((in[1] & 0xf0) >> 4)];
	out[2] = (uint8_t)(len > 1 ? cb64[((in[1] & 0x0f) << 2) | ((in[2] & 0xc0) >> 6)] : '=');
	out[3] = (uint8_t)(len > 2 ? cb64[in[2] & 0x3f] : '=');

	return;
}


size_t base64_encode_array (const uint8_t *instr, int size, uint8_t *outstr)
{
	uint8_t in[3], out[4];
	size_t	sizeRet;
	int i, len;
	sizeRet = 0;

	while (size >= 0)
	{
		len = 0;
		for (i = 0; i < 3; i++)
		{
			in[i] = 0;
			if (size > 0)
			{
				in[i] = *instr;
				len++;
				instr++;
			}
			size--;
		}
		if (len)
		{
			encodeblock (in, out, len);
			for (i = 0; i < 4; i++)
			{
				*outstr++ = out[i];
				sizeRet++;
			}
		}
	}
	return sizeRet;
}

/**
* decode 4 '6-bit' characters into 3 8-bit binary bytes
*/
static void decodeblock (uint8_t in[4], uint8_t out[3])
{
	out[0] = (uint8_t)(in[0] << 2 | in[1] >> 4);
	out[1] = (uint8_t)(in[1] << 4 | in[2] >> 2);
	out[2] = (uint8_t)(((in[2] << 6) & 0xc0) | in[3]);

	return;
}

/**
* decode a base64 encoded string (maybe broken...)
*/
int base64_decode (const uint8_t *instr, int size, uint8_t *outstr)
{
	uint8_t in[4], out[3];
	char in_raw[4];
	int i, len, v,removebyte;
	int	retSize;
	bool bFinalSequence;
	retSize = 0;
	removebyte = 0;
	bFinalSequence = false;

	while (size > 0)
	{
		for (len = 0, i = 0; i < 4 && (size >= 0); i++)
		{
			v = *instr++;
			in_raw[i] = v;
			if (bFinalSequence)
			{
				if (v != '=')
				{
					retSize = -1;
				}
			}
			if (retSize >= 0)
			{
				if (v == '=')
				{
					removebyte++;
					bFinalSequence = true;
					v = 0;
				}
				else
				{

					v = (uint8_t)((v < 43 || v > 122) ? -1 : cd64[v - 43]);
					if (v > 0)
					{
						v = (uint8_t)((v == '$') ? -1 : v - 61);
					}
					if (v < 0)
					{
						retSize = -1;
						break;
					}
				}
				size--;
			}

			if (retSize >= 0)
			{
				if (size >= 0)
				{
					len++;
					if (v)
					{
						in[i] = (uint8_t)(v - 1);
					}
				}
				else
				{
					in[i] = 0;
				}
			}
			else
			{
				break;
			}
		}
		if (i != 4)
		{
			retSize = -1;
		}
		if (retSize >= 0)
		{
			if (len)
			{
				decodeblock (in, out);
				for (i = 0; i < len - 1; i++)
				{
					*outstr++ = out[i];
					retSize++;
				}
			}
		}
		else
		{
			break;
		}
	}

	if (removebyte >= 3)
	{
		retSize = -1;
	}
	else
	if (removebyte >= 2)
	{
		retSize -= 2;
	}
	else
	if (removebyte >= 1)
	{
		retSize -= 1;
	}

	return retSize;
}
