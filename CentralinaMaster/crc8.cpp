#include <stdint.h>
#include <stddef.h>
#include "crc8.h"

uint8_t calcCRC8_2 (uint8_t bySEED, const  uint8_t* pbyBuffer, size_t uiSize)
{
	size_t	uiScanner;
	uint8_t	byRetCRC;
	uint8_t	byTemp;

	byRetCRC = bySEED;
	uiScanner = 0;

	while (uiScanner < uiSize)
	{
		byTemp = byRetCRC ^ pbyBuffer[uiScanner];

		byRetCRC = 0;

		if (byTemp & 0x01)
			byRetCRC ^= 0x5E;
		if (byTemp & 0x02)
			byRetCRC ^= 0xBC;
		if (byTemp & 0x04)
			byRetCRC ^= 0x61;
		if (byTemp & 0x08)
			byRetCRC ^= 0xC2;
		if (byTemp & 0x10)
			byRetCRC ^= 0x9D;
		if (byTemp & 0x20)
			byRetCRC ^= 0x23;
		if (byTemp & 0x40)
			byRetCRC ^= 0x46;
		if (byTemp & 0x80)
			byRetCRC ^= 0x8C;

		uiScanner++;
	}

	return byRetCRC;
}

static const uint8_t r1[16] = {
	0x00, 0x5e, 0xbc, 0xe2, 0x61, 0x3f, 0xdd, 0x83,
	0xc2, 0x9c, 0x7e, 0x20, 0xa3, 0xfd, 0x1f, 0x41,
};

const uint8_t r2[16] = {
	0x00, 0x9d, 0x23, 0xbe, 0x46, 0xdb, 0x65, 0xf8,
	0x8c, 0x11, 0xaf, 0x32, 0xca, 0x57, 0xe9, 0x74
};

uint8_t calcCRC8 (uint8_t bySEED, const uint8_t* pbyBuffer, size_t uiSize)
{
	while (uiSize)
	{
		bySEED = calcCRC8_BYTE (bySEED, *pbyBuffer);
		pbyBuffer++;
		uiSize--;
	}

	return bySEED;
}

uint8_t calcCRC8_BYTE (uint8_t bySEED, uint8_t byVAL)
{
	bySEED = bySEED ^ byVAL;
	bySEED = r1[(uint8_t)(bySEED & 0xf)] ^ r2[(uint8_t)(bySEED >> 4)];
	return bySEED;
}
