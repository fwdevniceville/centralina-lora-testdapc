#ifndef __CALCCRC8_H__
#define __CALCCRC8_H__

#define CRC8_START_SEED	(0xFF)

uint8_t calcCRC8 (uint8_t bySEED, const uint8_t* pData, size_t uiSize);
uint8_t calcCRC8_2 (uint8_t bySEED, const uint8_t* pData, size_t uiSize);
uint8_t calcCRC8_BYTE (uint8_t bySEED, uint8_t byVAL);

#endif

