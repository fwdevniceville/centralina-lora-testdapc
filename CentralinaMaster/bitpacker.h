#ifndef __BITPACKER_H__
#define __BITPACKER_H__


typedef struct tagBITPACKER
{
	const	uint8_t*		pBufferPop;
	uint8_t*				pBufferPush;
	uint16_t				uiArraySize;
	uint16_t				uiArrayPos;
	int16_t					iCurrBit;
	int16_t					iMaxBit;
}BITPACKER;


void bitpackerInitPop (BITPACKER* pBitPacker, const uint8_t* pBuffer, uint16_t uiSize);
void bitpackerInitPush (BITPACKER* pBitPacker, uint8_t* pBuffer, uint16_t uiSize);

void bitpackerPushIntValue (BITPACKER* pBitPacker,int iVal, uint16_t uiBitSize);
void bitpackerPushUIntValue (BITPACKER* pBitPacker,unsigned uiVal, uint16_t uiBitSize);

void bitpackerPopIntValue (BITPACKER* pBitPacker,int* piVal, uint16_t uiBitSize);
void bitpackerPopUIntValue (BITPACKER* pBitPacker,unsigned* puiVal, uint16_t uiBitSize);

bool bitpackerPushTestFreeBits (BITPACKER* pBitPacker, uint16_t uiBitSize);
unsigned bitpackerGetPushedBytes (const BITPACKER* pBitPacker);


#endif


