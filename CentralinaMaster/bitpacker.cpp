/*
* bitpacker.c
*
*  Created on: 26 mar 2018
*      Author: daniele_parise
*/


#include <stdbool.h>
#include <stdint.h>
#include "bitpacker.h"
#include <string.h>

void bitpackerInitPop (BITPACKER* pBitPacker, const uint8_t* pBuffer, uint16_t uiSize)
{
	pBitPacker->pBufferPop = pBuffer;
	pBitPacker->pBufferPush = NULL;
	pBitPacker->uiArraySize = uiSize;
	pBitPacker->uiArrayPos = 0;
	pBitPacker->iCurrBit = 0;
	pBitPacker->iMaxBit = uiSize * 8;

}

void bitpackerInitPush (BITPACKER* pBitPacker, uint8_t* pBuffer, uint16_t uiSize)
{
	memset (pBuffer, 0, uiSize);
	pBitPacker->pBufferPop = NULL;
	pBitPacker->pBufferPush = pBuffer;
	pBitPacker->uiArraySize = uiSize;
	pBitPacker->uiArrayPos = 0;
	pBitPacker->iCurrBit = 0;
	pBitPacker->iMaxBit = uiSize * 8;
}

void bitpackerPushIntValue (BITPACKER* pBitPacker, int iVal, uint16_t uiBitSize)
{
	bitpackerPushUIntValue (pBitPacker, (unsigned)iVal, uiBitSize);
}

bool bitpackerPushTestFreeBits (BITPACKER* pBitPacker, uint16_t uiBitSize)
{
	int iRemaining;
	iRemaining = pBitPacker->iMaxBit;
	iRemaining -= pBitPacker->iCurrBit;
	if (iRemaining >= uiBitSize)
	{
		return true;
	}
	return false;
}

unsigned bitpackerGetPushedBytes (const BITPACKER* pBitPacker)
{
	unsigned uiRetBytes;
	uiRetBytes = 0;
	if (pBitPacker->pBufferPush)
	{
		uiRetBytes = (pBitPacker->iCurrBit / 8);
		if (pBitPacker->iCurrBit % 8)
		{
			uiRetBytes++;
		}
	}
	return uiRetBytes;
}
void bitpackerPushUIntValue (BITPACKER* pBitPacker, unsigned uiVal, uint16_t uiBitSize)
{
	if (pBitPacker->pBufferPush && (uiBitSize > 0) && (uiBitSize <= (sizeof (unsigned) * 8)))
	{
		unsigned uiMASK;
		uint8_t		uiMAP_MASK;
		uint8_t		uiMAP_POS;
		uiMASK = 1 << (uiBitSize - 1);
		while (uiMASK)
		{
			if (pBitPacker->iCurrBit >= pBitPacker->iMaxBit)
			{
				break;
			}
			uiMAP_POS = pBitPacker->iCurrBit / 8;
			uiMAP_MASK = 0x80 >> (pBitPacker->iCurrBit % 8);
			if (uiVal & uiMASK)
			{
				pBitPacker->pBufferPush[uiMAP_POS] |= uiMAP_MASK;
			}

			pBitPacker->iCurrBit++;
			uiMASK >>= 1;
		}
	}

}

void bitpackerPopIntValue (BITPACKER* pBitPacker, int* piVal, uint16_t uiBitSize)
{
	bitpackerPopUIntValue (pBitPacker, (unsigned*)piVal, uiBitSize);
}

void bitpackerPopUIntValue (BITPACKER* pBitPacker, unsigned* puiVal, uint16_t uiBitSize)
{
	if (pBitPacker->pBufferPop && (uiBitSize > 0) && (uiBitSize <= (sizeof (unsigned) * 8)))
	{
		unsigned	uiMASK;
		unsigned	uiVal;
		uint8_t		uiMAP_MASK;
		uint8_t		uiMAP_POS;
		uiMASK = 1 << (uiBitSize - 1);
		uiVal = 0;
		while (uiMASK)
		{
			uiMAP_POS = pBitPacker->iCurrBit / 8;
			uiMAP_MASK = 0x80 >> (pBitPacker->iCurrBit %8);
			if (pBitPacker->iCurrBit < pBitPacker->iMaxBit)
			{
				if (pBitPacker->pBufferPop[uiMAP_POS] & uiMAP_MASK)
				{
					uiVal |= uiMASK;
				}
			}

			pBitPacker->iCurrBit++;
			uiMASK >>= 1;
		}
		*puiVal = uiVal;
	}
}

