#include "stdafx.h"
#include "CBitPackerUnpaker.h"
#include "base64.h"

CBitPackerUnpaker::CBitPackerUnpaker()
{
//	clear();
}


CBitPackerUnpaker::~CBitPackerUnpaker()
{
}

void CBitPackerUnpaker::SetPushBuffer (uint8_t* abyBuffer, size_t uiSize)
{
	bitpackerInitPush (&m_stBITPACKER, abyBuffer, uiSize);
}
void CBitPackerUnpaker::SetPopBuffer (const uint8_t* abyBuffer, size_t uiSize)
{
	bitpackerInitPop (&m_stBITPACKER, abyBuffer, uiSize);
}


void CBitPackerUnpaker::pushIntValue(int iVal, size_t uiBitSize)
{
	bitpackerPushIntValue (&m_stBITPACKER,iVal, uiBitSize);
}

void CBitPackerUnpaker::pushUIntValue(unsigned uiVal, size_t uiBitSize)
{
	bitpackerPushUIntValue (&m_stBITPACKER, uiVal, uiBitSize);
}

void CBitPackerUnpaker::popIntValue(int* piVal, size_t uiBitSize)
{
	bitpackerPopIntValue (&m_stBITPACKER, piVal, uiBitSize);
}

void CBitPackerUnpaker::popUIntValue(unsigned* puiVal, size_t uiBitSize)
{
	bitpackerPopUIntValue (&m_stBITPACKER, puiVal, uiBitSize);
}

