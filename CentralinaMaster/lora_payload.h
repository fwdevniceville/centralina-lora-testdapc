/*
 * lora_payload.h
 *
 *  Created on: 30 mag 2018
 *      Author: daniele_parise
 */

#ifndef TASKS_LORA_PAYLOAD_H_
#define TASKS_LORA_PAYLOAD_H_



#define CENTRALINA_LORAWAN_RESULT_OK				(0)
#define CENTRALINA_LORAWAN_INVALID_DATA				(1)
#define CENTRALINA_LORAWAN_RET_UPLINK_TOO_LONG		(2)









#define CENTRALINA_LORAWAN_PORT_CMD			(10)

// GENERAL DOWNLINK PAYLOAD FOR PORT 10
// PROGRESSIVE MESSAGE ID + COMMAND_1 + COMMAND_2 + ... + COMMAND_END (until payload end)
// CMD FORMAT:
// COMAND_CODE + related payload

// RELATIVE UPLINK
// PROGRESSIVE MESSAGE ID(same ad command) + RETURN_COMMAND_1 + RETURN_COMMAND_2 + ... + RETURN_COMMAND_END
// RETURN_CMD FORMAT:
// RETURN_COMAND_CODE + RESULT + related return payload  (RESULT == 0 ==> SUCCESS)


// Set board datetime
// COMAND_CODE(0x01) + YY_MM_DD_WD_HH_MM (6 BYTES)
// size 1 + 6 	Year(0..99)_Month(1..12)_Day(1..31)_WeekWay(1..7 ; 1 = Monday)_Hour(0..23)_Minutes(0..59)
#define CENTRALINA_LORAWAN_CMD_DATETIME			(0x01)

// get board datetime
// COMAND_CODE (0x41)
// size 1
#define CENTRALINA_LORAWAN_CMD_GET_BOARDTIME	(0x41)



//*********************************************************************

// Set scheduler's packed slot
// COMAND_CODE(0x04) + PACKED_SLOT + crc8(PACKED_SLOT)
// PACKED_SLOT: BYTE[0..7] from BYTE[0]BIT[7] to BYTE[7]BIT[0] : MSB byte order
// slot number 				: 7 bits 	valid (0..63)	reserved (64..127)
// mode						: 3 bits 	valid (0..3)	reserved (4..7) 0 == disabled, 1 == exact day, 2 == week's day, 3 = day's period
// year (without century)	: 7 bits 	valid (0..99)	0 if mode == 2
// month					: 4 bits 	valid (1..12) 	0 if mode == 2
// month's day				: 5 bits 	valid (1..31) 	0 if mode == 2
// period (days units)		: 8 bits 	valid (1..255)	bit mask if mode == 2 (one bits for week's day bit0 == monday)
// start hour				: 5 bits 	valid (0..23)
// start minutes			: 6 bits 	valid (0..59)
// runtime(minutes)			: 11 bits 	valid (1..1440)
// out_mask					: 8 bits	valid (bit mask)


// example:
// slot number				: BYTE[0]BIT[7..1]
// mode						: BYTE[0]BIT[0] + BYTE[1]BIT[7..6]
// year						: BYTE[1]BIT[5..0] + BYTE[2]BIT[7]
// mont						: BYTE[2]BIT[6..3]
// day						: BYTE[2]BIT[2..0] + BYTE[3]BIT[7..6]
// period					: BYTE[3]BIT[5..0] + BYTE[4]BIT[7..6]
// start hour				: BYTE[4]BIT[5..1]
// start minute				: BYTE[4]BIT[0] + BYTE[5]BIT[7..3]
// runtime					: BYTE[5]BIT[2..0] + BYTE[6]BIT[7..0]
// out_mask					: BYTE[7]

// example numeric
// PROGRESSIVE MESSAGE ID	: 2
// COMAND_CODE				: 0x04
// slot number				: 1
// mode						: 1
// year						: 18
// mont						: 5
// day						: 23
// period					: 0
// start hour				: 11
// start minute				: 34
// runtime					: 60
// out_mask					: 3
// CRC8						: 0x63
//
// HEX PAYLOAD				: 0x02,0x04,0x02,0x49,0x2D,0xC0,0x16,0x58,0x3C,0x03,0x63
// base64 PAYLOAD			: AgQCSS3AFlg8A2M=
// MQTT STRING
// application/1/node/0000000500040124/tx
// {"reference" : "jdi9je333sl29", "confirmed" : true, "fPort" : 10, "data" : "AgQCSS3AFlg8A2M=" }
//



// size 1 + 8 + 1
#define CENTRALINA_LORAWAN_CMD_PROGRAM_PACKED		(0x04)





// read seduler's packed slot
// COMAND_CODE(0x45) + slot number
// size 1 + 1
#define CENTRALINA_LORAWAN_CMD_GET_PROGRAM_PACKED	(0x44)

// Set scheduler's slot
// COMAND_CODE(0x04) + SLOT + crc8(SLOT)
// SLOT: BYTE[0..10] : MSB byte order
// slot number 				: uint8_t 	valid (0..63)	reserved (64..127)
// mode						: uint8_t 	valid (0..3)	reserved (4..7) 0 == disabled, 1 == exact day, 2 == week's day, 3 = day's period
// year (without century)	: uint8_t 	valid (0..99)	0 if mode == 2
// month					: uint8_t 	valid (1..12) 	0 if mode == 2
// month's day				: uint8_t 	valid (1..31) 	0 if mode == 2
// period (days units)		: uint8_t 	valid (1..255)	bit mask if mode == 2 (one bits for week's day bit0 == monday)
// start hour				: uint8_t 	valid (0..23)
// start minutes			: uint8_t 	valid (0..59)
// runtime(minutes)			: uint16_t 	valid (1..1440)  (MSB byte order)
// out_mask					: uint8_t	valid (bit mask)


// size 1 + 11 + 1
#define CENTRALINA_LORAWAN_CMD_PROGRAM				(0x05)

// read seduler's slot
// COMAND_CODE(0x45) + slot number
// size 1 + 1
#define CENTRALINA_LORAWAN_CMD_GET_PROGRAM			(0x45)

// erase seduler's slot
// COMAND_CODE(0x06) + slot number
// size 1 + 1
#define CENTRALINA_LORAWAN_CMD_ERASE_PROGRAM	(0x06)

// drive specific out
// COMAND_CODE(0x11) + out number + timeout for close (minutes)
// size 1 + 1 + 1
#define CENTRALINA_LORAWAN_CMD_MANUAL_OUT		(0x11)


// CMD_RET + DATA UPLINK
// PROGRESSIVE MESSAGE ID(same ad command) + RETURN_COMMAND_1 + RETURN_COMMAND_2 + ... + RETURN_COMMAND_END
// RETURN_CMD FORMAT:
// RETURN_COMAND_CODE + RESULT + related return payload  (RESULT == 0 ==> SUCCESS)
// RETURN_COMAND_CODE = COMAND_CODE | 0x80
// RESULT = error code, see below




#define CENTRALINA_LORAWAN_PORT_CMD_RET_DATA		(11)

// return value posted in uplink payload
// return code = relative command code masked in or with 0x80
#define CENTRALINA_LORAWAN_RET_MASK				(0x80)

// Set board datetime return
// COMAND_RET_CODE(0x81) + RESULT
// size 1 + 1 + 0
#define CENTRALINA_LORAWAN_RET_DATETIME			(CENTRALINA_LORAWAN_RET_MASK | CENTRALINA_LORAWAN_CMD_DATETIME)

// Get board datetime return
// COMAND_RET_CODE(0xC1) + RESULT + YY_MM_DD_WD_HH_MM (6 BYTES)
// size 1 + 6 	Year(0..99)_Month(1..12)_Day(1..31)_WeekWay(1..7 ; 1 = Monday)_Hour(0..23)_Minutes(0..59)
#define CENTRALINA_LORAWAN_RET_GET_BOARDTIME	(CENTRALINA_LORAWAN_RET_MASK | CENTRALINA_LORAWAN_CMD_GET_BOARDTIME)


// Set scheduler's packed slot return
// COMAND_RET_CODE(0x84) + RESULT
// size 1 + 1 + 0
#define CENTRALINA_LORAWAN_RET_PROGRAM_PACKED			(CENTRALINA_LORAWAN_RET_MASK | CENTRALINA_LORAWAN_CMD_PROGRAM_PACKED)

// Get scheduler's packed slot return
// COMAND_RET_CODE(0xC4) + RESULT + PACKED_SLOT + crc8(PACKED_SLOT)
// PACKED_SLOT: same as for CENTRALINA_LORAWAN_CMD_PROGRAM
// crc8(PACKED_SLOT): same as for CENTRALINA_LORAWAN_CMD_PROGRAM
// size 1 + 1 + 8 + 1
#define CENTRALINA_LORAWAN_RET_GET_PROGRAM_PACKED		(CENTRALINA_LORAWAN_RET_MASK | CENTRALINA_LORAWAN_CMD_GET_PROGRAM_PACKED)

// Set scheduler's packed slot return
// COMAND_RET_CODE(0x84) + RESULT
// size 1 + 1 + 0
#define CENTRALINA_LORAWAN_RET_PROGRAM_PACKED			(CENTRALINA_LORAWAN_RET_MASK | CENTRALINA_LORAWAN_CMD_PROGRAM_PACKED)

// Get scheduler's slot return
// COMAND_RET_CODE(0xC4) + RESULT + PACKED_SLOT + crc8(PACKED_SLOT)
// PACKED_SLOT: same as for CENTRALINA_LORAWAN_CMD_PROGRAM
// crc8(PACKED_SLOT): same as for CENTRALINA_LORAWAN_CMD_PROGRAM
// size 1 + 1 + 8 + 1
#define CENTRALINA_LORAWAN_RET_GET_PROGRAM_PACKED		(CENTRALINA_LORAWAN_RET_MASK | CENTRALINA_LORAWAN_CMD_GET_PROGRAM_PACKED)

// Erase scheduler's slot return
// COMAND_RET_CODE(0x86) + RESULT
// size 1 + 1 + 0
#define CENTRALINA_LORAWAN_RET_ERASE_PROGRAM	(CENTRALINA_LORAWAN_RET_MASK | CENTRALINA_LORAWAN_CMD_ERASE_PROGRAM)


// drive specific out return
// COMAND_RET_CODE(0x91) + RESULT
// size 1 + 1 + 0
#define CENTRALINA_LORAWAN_RET_MANUAL_OUT		(CENTRALINA_LORAWAN_RET_MASK | CENTRALINA_LORAWAN_CMD_MANUAL_OUT)


#endif /* TASKS_LORA_PAYLOAD_H_ */
