
// CentralinaMaster.h: file di intestazione principale per l'applicazione PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "includere 'stdafx.h' prima di includere questo file per PCH"
#endif

#include "resource.h"		// simboli principali


// CCentralinaMasterApp:
// Per l'implementazione di questa classe, vedere CentralinaMaster.cpp
//

class CCentralinaMasterApp : public CWinApp
{
public:
	CCentralinaMasterApp();

// Override
public:
	virtual BOOL InitInstance();

// Implementazione

	DECLARE_MESSAGE_MAP()
};

extern CCentralinaMasterApp theApp;
