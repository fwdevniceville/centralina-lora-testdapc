
// CentralinaMasterDlg.h: file di intestazione
//

#pragma once

#include "../../paho/inc/MQTTAsync.h"

// Finestra di dialogo CCentralinaMasterDlg
class CCentralinaMasterDlg : public CDialog
{
// Costruzione
public:
	CCentralinaMasterDlg(CWnd* pParent = NULL);	// costruttore standard
	virtual ~CCentralinaMasterDlg();

// Dati della finestra di dialogo
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CENTRALINAMASTER_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// supporto DDX/DDV


// Implementazione
protected:
	MQTTAsync mqtt_client;
protected:
	static int mqtt_client_message_recv(void* context, char* topicName, int topicLen, MQTTAsync_message* message);
protected:
	CString	m_strDEVUI;
	int		m_iMsgProgr;
protected:
	HICON m_hIcon;

	// Funzioni generate per la mappa dei messaggi
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedButtonSubscribe();
	afx_msg void OnStnClickedTextSubRx();
	afx_msg void OnBnClickedButtonSendTime();
	afx_msg void OnBnClickedButtonValveOpen();
	afx_msg void OnBnClickedButtonValveClose();
	afx_msg void OnBnClickedButtonScheduleRead();
	afx_msg void OnBnClickedButtonScheduleWrite();
	int SendLoRaWANMessage (const char* strDevEUI, const char* strRefMsg, bool bConfirmed, unsigned int uiPort, const uint8_t* abyPayloadBuffer, unsigned int uiPayloadSize);
};
